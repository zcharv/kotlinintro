object BeerSong {
    fun verses(first: Int, last: Int): String {
        val firstPart = " bottles of beer on the wall, "
        val middlePart = " bottles of beer.\nTake one down and pass it around, "
        val lastPart = " bottles of beer on the wall.\n\n"
        var returnString = ""
        for (i in first downTo last) {
            returnString += when {
                i > 2 -> i.toString() + firstPart + i + middlePart + i.minus(1) + lastPart
                i == 2 -> "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n\n"
                i == 1 -> "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n\n"
                i == 0 -> "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n\n"
                else -> ""
            }
        }
        return returnString.dropLast(1)
    }
}
